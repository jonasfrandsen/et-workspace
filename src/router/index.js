import { createWebHistory, createRouter } from "vue-router";
import MyWorkspace from "../views/MyWorkspace.vue";
import Folder from "../views/Folder.vue";

const routes = [
  { path: "/", name: "Home", component: MyWorkspace },
  { path: "/folders/:folderName", name: "Folder", component: Folder }
];

const router = new createRouter({
  history: createWebHistory(),
  routes
});

export default router;
