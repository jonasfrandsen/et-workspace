import { ref } from "vue";

const breadcrumbData = () => {
  const crumbsFolder = ref(["My Folders"]);
  const crumbsProject = ref(["My Projects"]);

  return { crumbsFolder, crumbsProject };
};

export default breadcrumbData;
