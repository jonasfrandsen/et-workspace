import { ref } from "vue";

//services
import fetchToken from "./fetchToken";

const fetchUser = () => {
  const data = ref({});
  const error = ref("");
  const loading = ref(false);

  const fetchUserData = async () => {
    loading.value = true;
    try {
      const token = await fetchToken();
      try {
        const res = await fetch(
          "https://api.platform.sandbox.easytranslate.com/api/v1/user",
          {
            method: "GET",
            headers: {
              Authorization: `${token.token_type} ${token.access_token}`,
              "Content-Type": "application/x-www-form-urlencoded"
            },
            redirect: "follow"
          }
        );
        if (!res.ok) {
          throw Error(`Error ${res.status} - No user found`);
        }
        data.value = await res.json();
        loading.value = false;
        return data.value;
      } catch (err) {
        console.log(err.message);
        loading.value = false;
        error.value = err.message;
      }
    } catch (err) {
      console.log(err);
    }
  };
  return { data, error, loading, fetchUserData };
};

export default fetchUser;
