import { ref } from "vue";

//services
import fetchToken from "./fetchToken";

const fetchSingleFolder = () => {
  const data = ref({});
  const error = ref("");
  const loading = ref(false);

  const fetchSingleFolderData = async (folderId) => {
    loading.value = true;
    try {
      const token = await fetchToken();
      try {
        const res = await fetch(
          `https://api.platform.sandbox.easytranslate.com/api/v1/teams/developer-account/folders/${folderId}`,
          {
            method: "GET",
            headers: {
              Authorization: `${token.token_type} ${token.access_token}`,
              "Content-Type": "application/x-www-form-urlencoded"
            },
            redirect: "follow"
          }
        );
        if (!res.ok) {
          throw Error(
            `Error ${res.status} - FolderCard data could not be retrieved`
          );
        }
        const resValue = await res.json();
        data.value = await resValue.data;
        loading.value = false;
      } catch (err) {
        console.log(err.message);
        loading.value = false;
        error.value = err.message;
      }
    } catch (err) {
      console.log(err.message);
      loading.value = false;
      error.value = err.message;
    }
  };
  return { data, error, loading, fetchSingleFolderData };
};

export default fetchSingleFolder;
