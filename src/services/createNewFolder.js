import { ref } from "vue";

//services
import fetchToken from "./fetchToken";

const createNewFolder = () => {
  let isModalActive = ref(false);
  const error = ref("");
  const loading = ref(false);

  const createFolder = async (input) => {
    loading.value = true;
    let dataObject = {};

    if (input) {
      dataObject = {
        data: {
          type: "project-folder",
          attributes: {
            name: `${input}`
          }
        }
      };
    }
    try {
      const token = await fetchToken();
      try {
        const res = await fetch(
          "https://api.platform.sandbox.easytranslate.com/api/v1/teams/developer-account/folders",
          {
            method: "POST",
            headers: {
              Authorization: `${token.token_type} ${token.access_token}`,
              "Content-Type": "application/json"
            },
            body: JSON.stringify(dataObject)
          }
        );
        if (!res.ok) {
          throw Error(`Error ${res.status} - Could not create folder`);
        }
        const resValue = await res.json();
        console.log("Success", resValue);
      } catch (err) {
        console.log(err.message);
        loading.value = false;
        error.value = err.message;
      } finally {
        loading.value = false;
        isModalActive.value = false;
      }
    } catch (err) {
      console.log(err.message);
    }
  };
  return { isModalActive, error, loading, createFolder };
};

export default createNewFolder;
