import { ref } from "vue";

//services
import fetchToken from "./fetchToken";

const fetchProjects = () => {
  const data = ref({});
  const error = ref("");
  const loading = ref(false);

  const fetchProjectsData = async (team) => {
    loading.value = true;
    try {
      const token = await fetchToken();
      try {
        const res = await fetch(
          `https://api.platform.sandbox.easytranslate.com/api/v1/teams/${team}/projects?filters[is_workspace]=true`,
          {
            method: "GET",
            headers: {
              Authorization: `${token.token_type} ${token.access_token}`,
              "Content-Type": "application/x-www-form-urlencoded"
            },
            redirect: "follow"
          }
        );
        if (!res.ok) {
          throw Error(`Error ${res.status} - No data found`);
        }
        const resValue = await res.json();
        data.value = await resValue.data;
        loading.value = false;
        return data.value;
      } catch (err) {
        console.log(err.message);
        loading.value = false;
        error.value = err.message;
      }
    } catch (err) {
      console.log(err.message);
      loading.value = false;
      error.value = err.message;
    }
  };
  return { data, error, loading, fetchProjectsData };
};

export default fetchProjects;
