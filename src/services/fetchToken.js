let urlencoded = new URLSearchParams();
urlencoded.append("client_id", process.env.VUE_APP_CLIENT_ID);
urlencoded.append("client_secret", process.env.VUE_APP_CLIENT_SECRET);
urlencoded.append("grant_type", process.env.VUE_APP_GRANT_TYPE);
urlencoded.append("username", process.env.VUE_APP_USERNAME);
urlencoded.append("password", process.env.VUE_APP_PASSWORD);
urlencoded.append("scope", process.env.VUE_APP_SCOPE);

const fetchToken = async () => {
  try {
    const res = await fetch(
      "https://api.platform.sandbox.easytranslate.com/oauth/token",
      {
        method: "POST",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        body: urlencoded
      }
    );
    if (!res.ok) {
      throw Error(`Error ${res.status} - No token found`);
    }
    const resValue = await res.json();
    return resValue;
  } catch (err) {
    console.log(err.message);
  }
};

export default fetchToken;
